Turn lights on or off using javascript/Node using the [cloudtuya library](https://github.com/unparagoned/cloudtuya).

## Setup

1. Install the cloudtuya library on your system.  The top of the script defines the path that it's looking for, adjust this for the actual path on your system.
2. Update keys.json to match your Tuya login information per the cloudtuya website information.
3. Update the http port to your liking.
4. Test the URL in curl or your browser, ie:  http://localhost:8111/?outlet=Office%20Light%201&status=on to turn a light called "Office Light 1" on using the default port of 8111.
5. Once it works, integrate it into your system via simple web calls or a site that can make web calls.