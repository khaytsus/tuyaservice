/**
 * Take light name and status arguments and execute them
 * using the cloudtuya library
 * https://github.com/unparagoned/cloudtuya
 * Example: http://localhost:8111/?outlet=Office%20Light%202&status=on
 */

// Location where files are stored
let path = '/var/www/html/rfoutlet/';

// Required API authentication information
let keys = path + 'keys.json';

// devicesFile is only used to evaluate what devices the service found
// for debugging, confirmation of device names, etc
let devicesFile = path + 'devices.json';

// http port
let port = '8111';

// Not much need to touch below here

// Load our libraries
const fs = require('fs');
const CloudTuya = require('/usr/local/lib/node_modules/cloudtuya');
const Light = require('/usr/local/lib/node_modules/cloudtuya/devices/light');
const http = require('http');
const url = require('url');

// Name of our Tuya library we use in a few places
const name = 'cloudtuya';

// API vars
let apiKeys = {};
let api = {};
let tokens = {};

// Device data var
let deviceData = {};

// The keys file contains the login information for Tuya's API
try {
    apiKeys = require(keys);
} catch (err) {
    console.error('keys.json is missing.');
}

// Schedule getting new tokens every 12 hours
var hours = 12,
    the_interval = hours * 60 * 60 * 1000;
setInterval(function() {
    console.log("I am doing my", hours, " hour update");
    getTokens();
}, the_interval);

// Connect to the API and get tokens 
getTokens();

// Go into our main function that waits for http connections
main();

// Retrieves both our API tokens as well as our devices
async function getTokens() {
    console.log('Inside getTokens()');

    // Init API from keys.json data
    api = new CloudTuya({
        userName: apiKeys.userName,
        password: apiKeys.password,
        bizType: apiKeys.bizType,
        countryCode: apiKeys.countryCode,
        region: apiKeys.region,
    });

    console.log('Getting API tokens');
    tokens = await api.login();

    console.log('Getting device data from API');
    deviceData = await api.find()

    console.log('Number of devices found:', deviceData.length);

    // Save device to device.json if we found a sane number
    if (deviceData.length > 0) {
        saveDataToFile(deviceData);
    } else {
        console.log('Did not save devices as we found 0');
    }
}

// Handles incoming connections and executing on them
async function main() {
    const requestListener = function(req, res) {
        const queryObject = url.parse(req.url, true).query;
        res.writeHead(200, {
            'Content-Type': 'text/html'
        })

        let lights = queryObject.outlet;
        if (lights) {
            lights = queryObject.outlet.split(",");
        }
        let lightstatus = queryObject.status;

        // If lights is refresh, just force a token/device refresh
        if (lights == 'refresh') {
            getTokens();
        }

        // If lights and status are defined, do the things
        if (lights !== 'refresh' && lights && lightstatus && deviceData) {
            changeStatus(lights, lightstatus);
        }
        res.end();
    }

    const server = http.createServer(requestListener);
    server.listen(port);
}

// Change the status of a device
async function changeStatus(lights, lightstatus) {
    // Interate over the list of lights
    lights.forEach(lightname => {
        // Find the ID of the light name from our stored data
        var device = deviceData.find(
            (it) => {
                return it.name === lightname;
            }
        );

        var device = deviceData.find(
            (it) => {
                return it.name === lightname;
            }
        );

        if (device) {
            // Setting new Device ID
            deviceID = device.id

            // Turn this light on/off as directed
            var myLight = new Light({
                api: api,
                deviceId: deviceID
            });

            if (lightstatus == 'on') {
                console.log('Turning', lightname, lightstatus);
                myLight.turnOn();
            }

            if (lightstatus == 'off') {
                console.log('Turning', lightname, lightstatus);
                myLight.turnOff();
            }
        } else {
            console.log('Unknown device: [', lightname, ']');
        }
    })
}

// Store our devices file to disk, primarily for analysis as we
// don't otherwise use this file
function saveDataToFile(data, file = devicesFile) {
    fs.writeFile(file, JSON.stringify(data), (err) => {
        if (err) {
            console.log(JSON.stringify(err, null, 4));
        }
        console.log('Devices file saved');
        return (file);
    });
}